import { Button, Card, CardActions, CardContent, Container, Grid, Typography } from "@mui/material"
import { useDispatch, useSelector } from "react-redux";

const MobileOrder = () => {
    // Khai báo hàm dispatch sự kiện tới redux store
    const dispatch = useDispatch();

    // useSelector để đọc state từ redux
    const { totalPrice, iphoneQuantity, rogTrixPhoneQuantity, ssPhoneQuantity } = useSelector((reduxData) => {
        return reduxData.MobileOrderReducer;
    })
    const buyIphoneClickHandler = () => {
        dispatch({
            type: "IPHONE_BUY_CLICKED"
        })
    }
    const buyRoxTrixClickHandler = () => {
        dispatch({
            type: "ROXTRIX_BUY_CLICKED"
        })
    }
    const buySamSungClickHandler = () => {
        dispatch({
            type: "SAMSUNG_BUY_CLICKED"
        })
    }
    return (
        <>
            <Container style={{ marginTop: "50px" }}>
                <Grid container spacing={10}>
                    <Grid item md={4} >
                        <Card>
                            <CardContent>
                                <Typography>
                                    Iphone 15 Promax
                                </Typography>
                                <Typography>
                                    Price: 900 USD
                                </Typography>
                                <Typography>
                                    Quantity: {iphoneQuantity}
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Button onClick={buyIphoneClickHandler} variant="contained" color="success" style={{ color: "orange" }}>
                                    Buy
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item md={4}>
                        <Card>
                            <CardContent>
                                <Typography>
                                    RogTrix Phone 8
                                </Typography>
                                <Typography>
                                    Price: 650 USD
                                </Typography>
                                <Typography>
                                    Quantity: {rogTrixPhoneQuantity}
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Button onClick={buyRoxTrixClickHandler} variant="contained" color="success" style={{ color: "orange" }}>
                                    Buy
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item md={4}>
                        <Card>
                            <CardContent>
                                <Typography>
                                    Samsung Galaxy Z Fold 5
                                </Typography>
                                <Typography>
                                    Price: 800 USD
                                </Typography>
                                <Typography>
                                    Quantity: {ssPhoneQuantity}
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Button onClick={buySamSungClickHandler} variant="contained" color="success" style={{ color: "orange" }}>
                                    Buy
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid>

                <h2>Total: {totalPrice} $</h2>
            </Container>

        </>
    )
}
export default MobileOrder