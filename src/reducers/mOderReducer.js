// Định nghĩa state khởi tạo cho table
const initialState = {
    totalPrice: 0,
    iphoneQuantity: 0,
    rogTrixPhoneQuantity: 0,
    ssPhoneQuantity: 0
}


const MobileOrderReducer = (state = initialState, action) => {
    switch (action.type) {
        case "IPHONE_BUY_CLICKED":
            state.iphoneQuantity = state.iphoneQuantity + 1;
            state.totalPrice = state.totalPrice + 900;

            break;

        case "ROXTRIX_BUY_CLICKED":
            state.rogTrixPhoneQuantity = state.rogTrixPhoneQuantity + 1;
            state.totalPrice = state.totalPrice + 650;

            break;
        case "SAMSUNG_BUY_CLICKED":
            state.ssPhoneQuantity = state.ssPhoneQuantity + 1;
            state.totalPrice = state.totalPrice + 800;

            break;

        default:
            break;
    }
    return { ...state }
}

export default MobileOrderReducer;